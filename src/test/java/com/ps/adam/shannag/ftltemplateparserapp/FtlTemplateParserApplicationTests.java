package com.ps.adam.shannag.ftltemplateparserapp;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ps.adam.shannag.ftltemplateparserapp.ftl.exceptions.InvalidLengthException;
import com.ps.adam.shannag.ftltemplateparserapp.ftl.exceptions.InvalidMessageException;
import com.ps.adam.shannag.ftltemplateparserapp.ftl.exceptions.InvalidTemplateException;
import com.ps.adam.shannag.ftltemplateparserapp.ftl.parser.FTLParser;
import com.ps.adam.shannag.ftltemplateparserapp.model.FtlParseRequest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.Properties;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@AutoConfigureMockMvc
@SpringBootTest
class FtlTemplateParserApplicationTests {

    private FTLParser ftlParser;

    private FtlParseRequest ftlParseRequest;

    private String output;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    ObjectMapper mapper;

    private static final MediaType APPLICATION_JSON_UTF8 = MediaType.APPLICATION_JSON;

    @BeforeEach
    public void init() {
        ftlParseRequest = new FtlParseRequest();
        loadProperties("test.data");
        ftlParser = new FTLParser();
    }

    @Test
    public void whenParse_ReturnOutput() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/api/parse")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(ftlParseRequest)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("@.output").exists())
                .andExpect(jsonPath("@.output").value(this.output));
    }

    @Test
    public void whenErrorOnParse_ReturnOutputWithStatusCode400() throws Exception {
        ftlParseRequest.setTemplate(ftlParseRequest.getTemplate().substring(0,5));
        mockMvc.perform(MockMvcRequestBuilders.post("/api/parse")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(ftlParseRequest)))
                .andExpect(status().is(400))
                .andExpect(content().contentType(APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("@.output").exists());
    }

    @Test
    public void whenFtlParse_ReturnOutputOf_ParsedMessage() {
        assertFalse(ftlParser.parse(ftlParseRequest.getMessage(),ftlParseRequest.getTemplate()).isEmpty(),
                "Output message should not be empty when parse() is called");
    }

    @Test
    public void whenFtlParse_AndInvalidMessage_ThrowInvalidMessageException(){
        ftlParseRequest.setMessage("123456ECC NOU Ax00000");
        assertThrows(InvalidMessageException.class,()-> ftlParser.parse(ftlParseRequest.getMessage(),ftlParseRequest.getTemplate()),"Should Throw InvalidMessageException");
    }

    @Test
    public void whenFtlParse_AndInvalidMessageLength_ThrowInvalidLengthException(){
        ftlParseRequest.setMessage(ftlParseRequest.getMessage()+" 1234567");
        assertThrows(InvalidLengthException.class,()-> ftlParser.parse(ftlParseRequest.getMessage(),ftlParseRequest.getTemplate()),"Should Throw InvalidLengthException");
    }

    @Test
    public void whenFtlParse_AndInvalidFTLTemplate_ThrowInvalidTemplateException(){
        ftlParseRequest.setTemplate(ftlParseRequest.getTemplate().substring(0,5));
        ftlParser = new FTLParser();
        assertThrows(InvalidTemplateException.class,()-> ftlParser.parse(ftlParseRequest.getMessage(),ftlParseRequest.getTemplate()),"Should Throw InvalidTemplateException");
    }

    private void loadProperties(String fileName){
        Properties testData = new Properties();
        try {
            testData.load(ClassLoader.getSystemClassLoader().getResourceAsStream(fileName));
            this.ftlParseRequest.setTemplate(testData.getProperty("ftl"));
            this.ftlParseRequest.setMessage(testData.getProperty("msg"));
            this.output=testData.getProperty("output");
        } catch (Exception e) {
            throw new RuntimeException("Invalid properties file");
        }
    }

}
