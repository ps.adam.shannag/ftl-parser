package com.ps.adam.shannag.ftltemplateparserapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FtlTemplateParserApplication {

    public static void main(String[] args) {
        SpringApplication.run(FtlTemplateParserApplication.class, args);
    }

}
