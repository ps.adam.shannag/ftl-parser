package com.ps.adam.shannag.ftltemplateparserapp.ftl.exceptions;

public class InvalidLengthException extends RuntimeException{
    public InvalidLengthException(String message) {
        super(message);
    }
}
