package com.ps.adam.shannag.ftltemplateparserapp.ftl.exceptions;

public class InvalidTemplateException extends RuntimeException{
    public InvalidTemplateException(String message) {
        super(message);
    }
}
