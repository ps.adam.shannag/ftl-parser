package com.ps.adam.shannag.ftltemplateparserapp.model;

public class FtlParseRequest {
    private String template;
    private String message;

    public String getTemplate() {
        return template;
    }

    public void setTemplate(String template) {
        this.template = template;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
