package com.ps.adam.shannag.ftltemplateparserapp.rest;

import com.ps.adam.shannag.ftltemplateparserapp.ftl.parser.FTLParser;
import com.ps.adam.shannag.ftltemplateparserapp.model.FtlParseRequest;
import com.ps.adam.shannag.ftltemplateparserapp.model.FtlParseResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class FtlParserAPI {

    private FTLParser ftlParser;

    @PostConstruct
    public void init(){
        ftlParser = new FTLParser();
    }
    @PostMapping("/api/parse")
    public ResponseEntity<?> parseFtl(@RequestBody FtlParseRequest ftl){
        FtlParseResponse ftlParseResponse = new FtlParseResponse();
        try{
            String responseMsg = ftlParser.parse(ftl.getMessage(),ftl.getTemplate());
            ftlParseResponse.setOutput(responseMsg);
            return ResponseEntity.ok(ftlParseResponse);
        }catch (Exception e){
            ftlParseResponse.setOutput("Error: "+e.getMessage());
            return ResponseEntity.status(400).body(ftlParseResponse);
        }
    }
}
