package com.ps.adam.shannag.ftltemplateparserapp.ftl.parser;

import com.ps.adam.shannag.ftltemplateparserapp.ftl.exceptions.InvalidLengthException;
import com.ps.adam.shannag.ftltemplateparserapp.ftl.exceptions.InvalidMessageException;
import com.ps.adam.shannag.ftltemplateparserapp.ftl.exceptions.InvalidTemplateException;

import java.io.FileNotFoundException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FTLParser {

    private final List<String> parsedCommands = new ArrayList<>();
    private final Map<String, Integer> map;
    private final List<String> order;
    private Map<String, String> extraValues;
    private int type;

    private void loadProperties(String fileName){
        Properties extraValues = new Properties();
        this.extraValues = new HashMap<>();
        try {
            extraValues.load(ClassLoader.getSystemClassLoader().getResourceAsStream(fileName));
            extraValues.forEach((key,value) -> this.extraValues.put(key.toString(),value.toString()));
        } catch (Exception e) {
            throw new RuntimeException("Invalid properties file");
        }
    }

    private void parseCommands(String templateStructure) {
        String regexCommands = "(?<=\\$\\{).+?(?=\\})";
        Pattern pattern = Pattern.compile(regexCommands, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(templateStructure);
        while (matcher.find()) {
            this.parsedCommands.add(matcher.group());
        }
        if (this.parsedCommands.isEmpty()) {
            regexCommands = "(?<=\").+?(?=\")";
            pattern = Pattern.compile(regexCommands, Pattern.CASE_INSENSITIVE);
            matcher = pattern.matcher(templateStructure);
            if (matcher.find()) {
                String[] command = matcher.group().split(",");
                for(String temp:command){
                    this.parsedCommands.add(temp.split("\\?")[0]);
                }
                this.type=1;
                return;
            }
            throw new InvalidTemplateException("Invalid ftl template file.");
        }
    }

    private String parseFromCommand(String command, String regex) {
        Pattern pattern = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(command);
        if (matcher.find()) return matcher.group();
        String padParam = "?right_pad(" + extraValues.get(command.split("\\.")[1]).split(",")[0] + ",'" +
                extraValues.get(command.split("\\.")[1]).split(",")[1].replace("_"," ") + "')";
        command += padParam;
        matcher = pattern.matcher(command);
        if (matcher.find()) return matcher.group();
        throw new InvalidTemplateException("Invalid ftl Template at: " + command + ".");
    }

    private String getProperty(String command) {
        String regexProperty = "(?<=\\.).+?(?=\\?)";
        if(this.type==1)
            return command.split("\\[")[0];
        return parseFromCommand(command, regexProperty);
    }

    private Integer getLength(String command) {
        try {
            String regexLength = "(?<=\\().+?(?=\\,)";
            if(this.type==1)
                regexLength = "(?<=\\[).+?(?=\\])";
            return Integer.parseInt(parseFromCommand(command, regexLength));
        } catch (Exception exc) {
            return -999;
        }
    }


    public FTLParser() {
        this.map = new HashMap<>();
        this.order = new ArrayList<>();
        this.type =0;
        this.loadProperties("extra.values");
    }

    /**
     * @param msg the message to parse using the ftl template structure
     */
    public String parse(String msg, String templateStructure) {
        this.parseCommands(templateStructure);
        for (String command : this.parsedCommands) {
            this.order.add(this.getProperty(command));
            this.map.put(this.getProperty(command), this.getLength(command));
        }
        return parseMsg(msg);
    }

    private String parseMsg(String msg) {
        StringBuilder properties = new StringBuilder();
        int prevLen = 0;
        for (String temp : this.order) {
            int len;
            len = this.map.get(temp);
            try {
                String propertyValue = msg.substring(prevLen, len + prevLen);
                if(!temp.contains("$ignore$")){
                    properties.append(temp).append(" = ").append(propertyValue).append("\n");
                }
                prevLen += len;
            } catch (IndexOutOfBoundsException e) {
                throw new InvalidMessageException("Invalid message length at property: " + temp);
            }
        }
        if (msg.length() - prevLen != 0) {
            throw new InvalidLengthException("Parsed data length doesn't match the actual message length!\nMessage length: "
                    + msg.length() + "\nExtracted length: " + prevLen);
        }
        return properties.toString();
    }
}