package com.ps.adam.shannag.ftltemplateparserapp.model;

public class FtlParseResponse {
    private String output;

    public String getOutput() {
        return output;
    }

    public void setOutput(String output) {
        this.output = output;
    }
}
