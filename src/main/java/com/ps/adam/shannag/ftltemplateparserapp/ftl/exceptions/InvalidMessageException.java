package com.ps.adam.shannag.ftltemplateparserapp.ftl.exceptions;

public class InvalidMessageException extends RuntimeException{
    public InvalidMessageException(String message) {
        super(message);
    }
}
