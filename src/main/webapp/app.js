let hasResponse=false;
const toggleUI = ()=>{
    document.getElementById('responseBody').hidden = !hasResponse;
}
// Handle file upload with js
const loadFileAsText = (file, textArea) => {
    const fileToLoad = document.getElementById(file).files[0];
    const fileReader = new FileReader();
    fileReader.onload = function (fileLoadedEvent) {
        document.getElementById(textArea).value = fileLoadedEvent.target.result;
    };
    fileReader.readAsText(fileToLoad, "UTF-8");
}

const post = async () => {
    const data = {
        template: document.getElementById('ftlText').value,
        message: document.getElementById('msgText').value
    }
    try {
        const request = await fetch('http://localhost:8080/api/parse', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(data),
        });
        const response = await request.json();
        hasResponse=true;
        setUIResponse(response.output);
    } catch (error) {
        console.error('Error:', error);
    }
}

const setUIResponse = (text) => {
    document.getElementById('response').innerHTML = `
<code id="msgContent">${text}</code>`;
    toggleUI();
}

function copy() {
    const copyText = document.getElementById("msgContent");
    /* Copy the text inside the text field */
    navigator.clipboard.writeText(copyText.innerText).then(() => alert("Text copied!"));
}

toggleUI();


